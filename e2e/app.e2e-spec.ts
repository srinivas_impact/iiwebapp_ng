import { IiwebappNgPage } from './app.po';

describe('iiwebapp-ng App', function() {
  let page: IiwebappNgPage;

  beforeEach(() => {
    page = new IiwebappNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
